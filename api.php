<?php

require_once "config.php";

if($_GET["city"]){	
	$sql = "select locName from locations";
	$result = $conn->query($sql);
	$res = [];
	foreach ($result as $row){
		$res[] = $row['locName'];
	}
	$resultFin['listings'] = $res;
	print_r(json_encode($resultFin));
}
else {
	$location = $_GET["l"];
	$price_min = $_GET["pmin"];
	$price_max = $_GET["pmax"];
	$type = $_GET["t"];
	
	$sql = "select * from 
		properties as p
		left join locations as l
		on p.locationid =l.locId
		left join images as i
		on p.imageId = i.imgId";
	if($location || ($price_min && $price_min) || $type){
		$sql = $sql." where ";
		$where = [];
		if($location){
			array_push($where, " l.locName = '".$location."' ");
		}
		if ($price_min && $price_max){
			array_push($where, " p.rent between ".$price_min." and ".$price_max);
		}
		if($type){
			array_push($where, " p.type = '".$type."' ");
		}
	
		$condition = implode(" and ",$where);
		$sql = $sql.$condition;
	}
			
	$return_arr = array();
	$result = $conn->query($sql);
	
	foreach ($result as $row) {
		$row_array['name'] = $row['name'];
		$row_array['address'] = $row['address'];
		$row_array['rent'] = $row['rent'];
		$row_array['area'] = $row['area'];
		$row_array['type'] = $row['type'];
			
		$amenities  = $row['amenities'];
		$pieces = explode(",", $amenities);
		$row_array['amenities'] = $pieces;
	
		$row_array['locName'] = $row['locName'];
		$row_array['thumb'] = $BASE_URL_IMG."thumb/".$row['thumb'];
		$row_array['sphereUrl'] = $BASE_URL_IMG."sphereUrl/".$row['sphereUrl'];
		
		$innerResult = [];
		$groupId = $row['groupId'];
		$nestedResult = $conn->query("SELECT * FROM 3dList where groupId =".$groupId);
		foreach ($nestedResult as $innerRow) {
			$innerRow['markerId'] = $innerRow['d3Id'];
			unset($innerRow['d3Id']);
			
			$url = $BASE_URL_IMG."sphereUrl/".$innerRow['imageUrl'];
			$innerRow['imageUrl'] = $url;
			
			$cid  = $innerRow['childId'];
			$pieces = explode(",", $cid);
			$innerRow['childId'] = $pieces;
					
			array_push($innerResult, $innerRow);
		}
		$row_array["children"] = $innerResult;
		array_push($return_arr,$row_array);
	}
	$resultFin['listings'] = $return_arr;
	print_r(json_encode($resultFin));
}
?>